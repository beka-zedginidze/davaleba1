<?php

use Illuminate\Support\Facades\Route;


Route::get('/home', '\App\Http\Controllers\Pagecontroller@getHomePage');
Route::get('/contact', '\App\Http\Controllers\Pagecontroller@getContactPage');
Route::get('/about-us', '\App\Http\Controllers\Pagecontroller@aboutUsPage');
