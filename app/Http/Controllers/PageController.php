<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function getHomePage() {
        return view('home')->with('text', 'text from view');
    }
    public function getContactPage() {
        return view('contact');
    }
    public function aboutUsPage() {
        return view('about-us');
    }
}
