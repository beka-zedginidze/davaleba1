@extends('layouts.main', [
    'title' => 'Layout Title',
    'class' => 'footer',
    'show_footer' => true
])

@section('content')
<h1>{{ $text }}</h1>
<h1>This is section content</h1>
@endsection


@section('footer')
<footer style="position: absolute; height: 60px; width: 100%; bottom: 0; background-color: gray; color:white;">
    FOOTER
</footer>
@endsection